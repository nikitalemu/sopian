#!/bin/bash

NUM_CPU_CORES=$(nproc --all)

cpulimit -e "biandewi" -l $((80 * $NUM_CPU_CORES))